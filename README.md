Desktop Linux Hardening
=======================

References to harden a desktop Linux.


License: CC0
------------

[COPYING](COPYING)

```
Desktop Linux Hardening - References to harden a desktop Linux.

Written in 2020 by rusty-snake

To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.

You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
```
