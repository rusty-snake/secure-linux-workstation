dnf
===

The most tutorial will now say that you sould remove unneeded software.
For a server with constant jobs makes this sens, but desktop system ofter have a more
dynamic job, therefore you should onle remove packages where you know that you will never need them.

disable installing of optional dependencies
--------------------------------------------

The most package manager have optional dependencies like pacmans optional dependenices or
apts suggestions and recommendations. rpm/dnf call these dependencies weak-dependencies.

In order to disable installing of these dependenices,
set `install_weak_deps=False` in `/etc/dnf/dnf.conf`.
see [etc/dnf/dnf.conf](../etc/dnf/dnf.conf).

In order to enable installing of weak dependencies temporary you can use setopt:
`sudo dnf --setopt=install_weak_deps=True install foo`

restrict third-party repositories
---------------------------------

If you have third-party repositories like vivaldi or google-chrome you can restrict the package-names that can be installed from these repositores.

Example:

1. list all packages from the repository: `dnf repository-packages vivaldi list`
2. add `includepkgs=vivaldi-stable,vivaldi-snapshot` to `/etc/yum.repos.d/vivaldi.repo`

review transaction script from manual downloaded rpms
------------------------------------------------------

If you manualy download a rpm like vivaldi or google-chrome, you should review the transacrion scripts.
`rpm -qp --scripts path/to/foo.rpm`

If you want to install the rpm without executing these script, you can do this with:
`sudo dnf --setopt=tsflags=noscripts install path/to/foo.rpm`

See Also
--------

Notes
-----

`man:dnf.conf(5)`
`man:dnf(8)`
