coredump
========

Processing coredumps can be very resource-intensive.
Furthermore, coredumps can contain sensitive information such as passwords or similar.
Therefore you should turn them off.

You have to ways to disable coredumps:

disable coredumps completely
----------------------------

Let `/bin/false` "process" the coredumps.

Copy [etc/sysctl.d/disable-coredumps.conf] to `/etc/sysctl.d/disable-coredumps.conf`.

[etc/sysctl.d/disable-coredumps.conf]: ../etc/sysctl.d/disable-coredumps.conf

disable storing of coredumps
----------------------------

Leave `kernel.core_pattern` but disable processing of coredumps and backtraces.
This has the advantage that non-sensitive information about the crash are still written to the journal.

Copy [etc/systemd/coredump.conf] to `/etc/systemd/coredump.conf`.

[etc/systemd/coredump.conf]: ../etc/systemd/coredump.conf

See Also
--------

Notes
-----

 - <https://wiki.archlinux.org/index.php/Core_dump>
 - <https://www.freedesktop.org/software/systemd/man/systemd-coredump.html#Disabling%20coredump%20processing>
 - <https://www.freedesktop.org/software/systemd/man/coredump.conf.html>
 - <https://www.kernel.org/doc/html/latest/admin-guide/sysctl/kernel.html#core-pattern>
